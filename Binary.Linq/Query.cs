﻿using Binary.Linq.BL.API;
using Binary.Linq.BL.Models;
using Binary.Linq.BL.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Binary.Linq
{
    public class Query
    {
        public List<ProjectModel> ProjectsModel;
        public List<Project> Projects;
        public List<Task> Tasks;
        public List<Team> Teams;
        public List<User> Users;
        ProjectAPI api;
        public Query()
        {
            ProjectsModel = new();
            api = new();
            LoadData();
        }
        private void LoadData()
        {
            Projects = api.GetProjectsAsync().Result;
            Tasks = api.GetTasksAsync().Result;
            Teams = api.GetTeamsAsync().Result;
            Users = api.GetUsersAsync().Result;
            ProjectsModel = (from project in Projects
                        join author in Users on project.AuthorId equals author.Id
                        join team in Teams on project.TeamId equals team.Id
                        select new ProjectModel
                        {
                            Project = project,
                            Tasks = (from task in Tasks
                                     join performer in Users on task.PerformerId equals performer.Id
                                     where task.ProjectId == project.Id
                                     select new TaskModel
                                     {
                                         Task = task,
                                         Performer = performer
                                     }).ToList(),
                            Author = author,
                            Team = team
                        }).ToList();
        }

        //1. Отримати кількість тасків у проекті конкретного користувача (по id) (словник, де key буде проект, а value кількість тасків).
        public void GetCountTasksUserInProjects(int userId)
        {
            var tasks = ProjectsModel.Select(p => new
            {
                project = p,
                count = p.Tasks.Count(x => x.Performer.Id == userId)
            }).ToDictionary(k => k.project, v => v.count);
            
            foreach(var task in tasks)
            {
                Console.WriteLine($"{task.Key.Project.Name} --- {task.Value}");
            }
        }

        //2. Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів (колекція з тасків).
        public void GetTasksByUserId(int userId)
        {
            var tasks = ProjectsModel.SelectMany(t => t.Tasks).Where(x => x.Performer.Id == userId && x.Task.Name.Length < 45).ToList();
            foreach(var task in tasks)
            {
                Console.WriteLine($"Task: {task.Task.Name}  ---  Description: {task.Task.Description}");
            }
        }

        //3. Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2021) році 
        //для конкретного користувача (по id).
        public void GetFinishedTaskByUserId(int userId)
        {
            var tasks = ProjectsModel.SelectMany(t => t.Tasks)
                    .Where(x => x.Performer.Id == userId && x.Task.FinishedAt.GetValueOrDefault().Year == 2021)
                    .Select(x => new { id = x.Task.Id, name = x.Task.Name })
                    .ToList();
            foreach (var task in tasks)
            {
                Console.WriteLine($"ID: {task.id}  ---  Name: {task.name}");
            }
        }

        //4. Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років,
        //відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
        //P.S -в цьому запиті допускається перевірка лише року народження користувача, без прив'язки до місяця/дня/часу народження.
        public void GetTeams()
        {
            var teams = (from team in Teams
                         group team by team into t
                         select new
                         {
                             id = t.Key.Id,
                             TeamName = t.Key.Name,
                             Users = Users.Where(u => u.BirthDay.Year < 2011 && u.TeamId == t.Key.Id)
                                          .OrderByDescending(x => x.RegisteredAt).ToList()
                         }).ToList();
            foreach (var team in teams)
            {
                Console.WriteLine($"ID: {team.id}  ---  Name: {team.TeamName}\nUsers:\n");
                foreach(var user in team.Users)
                {
                    Console.WriteLine($"{user.FirstName} {user.LastName} {user.Email}");
                }
                Console.WriteLine("\n");
            }

        }

        //5. Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими tasks
        //по довжині name (за спаданням).
        public void GetUserOrderByNameWithTask()
        {
            var users = (from user in Users
                        orderby user.FirstName
                        select new
                        {
                            User = user,
                            Tasks = Tasks.Where(t => t.PerformerId == user.Id).OrderByDescending(x => x.Name.Length).ToList()
                        }).ToList();
            foreach (var user in users)
            {
                Console.WriteLine($"{user.User.FirstName} {user.User.LastName} {user.User.Email}\nTasks:");
                foreach (var task in user.Tasks)
                {
                    Console.WriteLine($"{task.Name}  ---  {task.Description}");
                }
                Console.WriteLine("\n");
            }

        }

        //6. Отримати наступну структуру (передати Id користувача в параметри):
        //  User
        // Останній проект користувача(за датою створення)
        // Загальна кількість тасків під останнім проектом
        // Загальна кількість незавершених або скасованих тасків для користувача
        // Найтриваліший таск користувача за датою(найраніше створений - найпізніше закінчений)
        //P.S. - в даному випадку, статус таска не має значення, фільтруємо тільки за датою.
        public void GetTask6(int userId)
        {
            var t = Tasks.Where(x => x.FinishedAt is not null).GroupBy(x => x.PerformerId);
            var i = Tasks.Where(x => x.PerformerId == userId && x.FinishedAt is not null).OrderByDescending(x => x.FinishedAt - x.CreatedAt);

            var str = (from user in Users
                       where user.Id == userId
                       let project = ProjectsModel.Where(x => x.Author.Id == user.Id)
                                                  .OrderByDescending(x => x.Project.CreatedAt)
                                                  .FirstOrDefault() ?? new ProjectModel()
                       select new Task6
                       {
                           User = user,
                           LastProjectUser = project.Project ?? null,
                           TotalTaskInLastProject = project.Tasks?.Count ?? 0,
                           TotalCountUnfinishedTask = ProjectsModel.SelectMany(x => x.Tasks)
                                                                   .Where(y => y.Task.State == TaskState.Third && y.Task.PerformerId == user.Id)
                                                                   .Count(),
                           LongestUserTask = ProjectsModel.SelectMany(x => x.Tasks)
                                                          .Where(x => x.Task.PerformerId == userId && x.Task.FinishedAt is not null)
                                                          .OrderByDescending(x => x.Task.FinishedAt - x.Task.CreatedAt)
                                                          .Select(x => x.Task)
                                                          .FirstOrDefault()

                       }).FirstOrDefault();
        }

    }

    public class Task6
    {
        public User User { get; set; }
        public Project LastProjectUser { get; set; }
        public int TotalTaskInLastProject { get; set; }
        public int TotalCountUnfinishedTask { get; set; }
        public Task LongestUserTask { get; set; }
    }
}
