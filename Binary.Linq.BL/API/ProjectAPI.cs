﻿using Binary.Linq.BL.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace Binary.Linq.BL.API
{
    public class ProjectAPI
    {
        private HttpClient client;
        JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true
        };
        public ProjectAPI()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(Config.BaseUri);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public async Task<List<Project>> GetProjectsAsync()
        {
            List<Project> projects = null;
            HttpResponseMessage response = await client.GetAsync("/api/Projects");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                projects = JsonSerializer.Deserialize<List<Project>>(i, options);
            }
            return projects;
        }
        public async Task<Project> GetProjectById(int id)
        {
            Project project = null;
            HttpResponseMessage response = await client.GetAsync($"/api/Projects/{id}");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                project = JsonSerializer.Deserialize<Project>(i, options);
            }
            return project;
        }

        public async Task<List<Models.Task>> GetTasksAsync()
        {
            List<Models.Task> tasks = null;
            HttpResponseMessage response = await client.GetAsync("/api/Tasks");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                tasks = JsonSerializer.Deserialize<List<Models.Task>>(i, options);
            }
            return tasks;
        }
        public async Task<Models.Task> GetTaskById(int id)
        {
            Models.Task task = null;
            HttpResponseMessage response = await client.GetAsync($"/api/Tasks/{id}");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                task = JsonSerializer.Deserialize< Models.Task> (i, options);
            }
            return task;
        }
        public async Task<List<Team>> GetTeamsAsync()
        {
            List<Team> teams = null;
            HttpResponseMessage response = await client.GetAsync("/api/Teams");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                teams = JsonSerializer.Deserialize<List<Team>>(i, options);
            }
            return teams;
        }
        public async Task<Team> GetTeamByIdAsync(int id)
        {
            Team team = null;
            HttpResponseMessage response = await client.GetAsync($"/api/Teams/{id}");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                team = JsonSerializer.Deserialize<Team>(i, options);
            }
            return team;
        }
        public async Task<List<User>> GetUsersAsync()
        {
            List<User> users = null;
            HttpResponseMessage response = await client.GetAsync("/api/Users");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                users = JsonSerializer.Deserialize<List<User>>(i, options);
            }
            return users;
        }
        public async Task<User> GetUserByIdAsync(int id)
        {
            User user = null;
            HttpResponseMessage response = await client.GetAsync($"/api/Users/{id}");
            if (response.IsSuccessStatusCode)
            {
                var i = await response.Content.ReadAsStringAsync();
                user = JsonSerializer.Deserialize<User>(i, options);
            }
            return user;
        }

    }
}
