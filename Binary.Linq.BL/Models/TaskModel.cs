﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Binary.Linq.BL.Models
{
    public class TaskModel
    {
        public Task Task { get; set; }
        public User Performer { get; set; }
    }
}
